import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Diario from '../views/Diario.vue'
import Avaliacao from '../views/Avaliacao.vue'
import CadAluno from '../views/CadAluno.vue'
import CadProf from '../views/CadProf.vue'
import CadTurma from '../views/CadTurma.vue'
import CadTurmaAluno from '../views/CadTurmaAluno.vue'
import Bimestre from '../views/Bimestre.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/diario',
    name: 'Diario',
    component: Diario
  },
  {
    path: '/avaliacao',
    name: 'Avaliaçao',
    component: Avaliacao
  },
  {
    path: '/cadastro-aluno',
    name: 'Cadastro Aluno',
    component: CadAluno
  },
  {
    path: '/cadastro-prof',
    name: 'Cadastro Professor',
    component: CadProf
  },
  {
    path: '/cadastro-turma',
    name: 'Cadastro Turma',
    component: CadTurma
  },
  {
    path: '/bimestre',
    name: 'Bimestre',
    component: Bimestre
  },
  {
    path: '/turma-aluno',
    name: 'TurmaAluno',
    component: CadTurmaAluno
  }
]

const router = new VueRouter({
  routes
})

export default router
